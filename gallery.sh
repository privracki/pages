#!/bin/sh
echo "\
<!DOCTYPE html>
<html>
	<head>
	<style>
		body {
			background-color: black;
			max-width: 60em;
			margin: auto;
			text-align: center;
		}
		img {
			width: 10em;
		}
	</style>
	</head>
	<body>"

for file in *; do
	mkdir -p thumb
	tag="\t\t<a href="$file"><img src=\"thumb/$file\"></a>"
	case $file in
		*jpg) echo $tag
			if ! [ "$1" = "-n" ]; then
				convert -resize 640x360 "$file" "thumb/$file"
			fi;;
		*png) echo $tag
			if ! [ "$1" = "-n" ]; then
				convert -resize 640x360 "$file" "thumb/$file"
			fi;;
	esac
done

echo "\
	</body>
</html>"
